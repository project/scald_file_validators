<?php
/**
 * Implements hook_form().
 */
function scald_file_validators_config_form($form, &$form_state) {

    $modules = module_implements('scald_add_form');

    foreach (scald_atom_providers() as $providers) {
        foreach ($providers as $provider => $provider_name) {
            if (in_array($provider, $modules)) {
                $provider_form = $provider_form_state = array();
                $function = $provider.'_scald_add_form';
                $function($provider_form, $provider_form_state);
                if (isset($provider_form['file']['#upload_validators'])) {
                    $form[$provider] = array(
                        '#type' => 'fieldset',
                        '#title' => t(ucwords($provider_name)),
                        '#collapsible' => TRUE,
                        '#collapsed' => FALSE,
                        '#description' => t('<b>If no values are entered, the default values of the providers will be assumed.</b>'),
                    );
                    $form[$provider]['scald_file_validators_maximum_size_'.$provider] = array(
                        '#type' => 'textfield',
                        '#title' => t('Maximum size'),
                        '#default_value' => variable_get('scald_file_validators_maximum_size_'.$provider, NULL),
                        '#size' => 60,
                        '#description' => t('An integer specifying the maximum file size in megabytes (MB). Zero indicates that no limit should be enforced.'),
                        '#required' => FALSE,
                        '#element_validate' => array('scald_file_validators_maximum_size_validate'),
                    );
                    $form[$provider]['scald_file_validators_extensions_'.$provider] = array(
                        '#type' => 'textfield',
                        '#title' => t('Allowed extensions'),
                        '#default_value' => variable_get('scald_file_validators_extensions_'.$provider, NULL),
                        '#size' => 60,
                        '#description' => t('A string with a space separated list of allowed extensions.'),
                        '#required' => FALSE,
                    );
                }
            }
        }
    }

    return system_settings_form($form);
}

/**
 * Custom form validators
 */
function scald_file_validators_maximum_size_validate($element, $form_state) {
    $maximum_size = $form_state['values'][$element['#name']];
    if ($maximum_size != NULL && !is_numeric($maximum_size)) {
        form_error($element, t('Maximum file upload size must be a integer.'));
    }
    if (($maximum_size * pow(1024, 2)) > file_upload_max_size()) {
        form_error($element, t('Maximum file upload size in PHP settings is %size MB.',
            array('%size' => (file_upload_max_size() / pow(1024, 2))))
        );
    }
}
